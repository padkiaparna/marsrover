﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover.Core;
using MarsRover.Core.Commands;
using MarsRover.Core.Direction;
using MarsRover.Core.Interfaces;

namespace MarsRover
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Enter 'exit' to quit");
                Console.WriteLine("****Input file path*****");
                string input = Console.ReadLine();
                if (input.Equals("exit", StringComparison.OrdinalIgnoreCase)) break;
                string filePath = "../../input.txt";
                if (File.Exists(input)) filePath = input;
                try
                {
                    string[] inputLines = File.ReadAllLines(filePath);
                    var upperRightCoOrdinates = inputLines[0];
                    var x = Convert.ToInt32(upperRightCoOrdinates.Split(' ')[0]);
                    var y = Convert.ToInt32(upperRightCoOrdinates.Split(' ')[1]);
                    var plateau = new Plateau(x, y);
                    IRover rover = new Rover(plateau);
                    for (int i=1; i< inputLines.Length; i++)
                    {
                        new CommandBuilder(inputLines[i]).SetRover(plateau,rover);
                        i = i+1;
                        rover.ExecuteCommands(inputLines[i]);
                        Console.WriteLine();
                        Console.WriteLine(rover.ToString());
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }
        }

        private static void WriteHelp()
        {
            Console.WriteLine("Valid Commands are:");
            Console.WriteLine("L - Turn Rover Left");
            Console.WriteLine("R - Turn Rover Right");
            Console.WriteLine("F - Move Rover Forward");
            Console.WriteLine("E - End");
            Console.WriteLine("Enter to execute commands");
        }
    }
}
