﻿using MarsRover.Core.Commands;
using MarsRover.Core.Direction;
using MarsRover.Core.Interfaces;

namespace MarsRover.Core
{
    public class Rover : IRover
    {
        private IPlateau _plateau;

        public IDirection Direction { get; private set; }

        public Rover(IPlateau plateau)
        {
            _plateau = plateau;
            Direction = new North(_plateau);
        }

        public void TurnLeft()
        {
            Direction = Direction.TurnLeft();
        }

        public void TurnRight()
        {
            Direction = Direction.TurnRight();
        }

        public void Forward()
        {
            Direction.Move();
        }


        public int CoordinateX
        {
            get { return _plateau.CoordinateX; }
        }

        public int CoordinateY
        {
            get { return _plateau.CoordinateY; }
        }

        public override string ToString()
        {
            return string.Format("{0} {1} {2}", CoordinateX, CoordinateY, Direction);
        }


        public void ExecuteCommands(string commandString)
        {

            for (int index = 0; index < commandString.Length; index++)
            {
                var command = commandString[index].ToString();
                var commandToExecute = new CommandBuilder(command).GetCommand();
                commandToExecute.Execute(this);
            }
        }

        public void SetPosition(IPlateau plateau, IDirection direction)
        {
            _plateau = plateau;
            Direction = direction;
        }
    }
}
