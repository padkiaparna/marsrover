﻿using MarsRover.Core.Interfaces;

namespace MarsRover.Core
{
    public class Plateau : IPlateau
    {
        public int CoordinateX { get;  set; }
        public int CoordinateY { get;  set; }

        private readonly int _topRightCoordinateX;
        private readonly int _topRightCoordinateY;

        public Plateau(int topRightCoordinateX, int topRightCoordinateY)
        {
            CoordinateX = 0;
            CoordinateX = 0;
            _topRightCoordinateX = topRightCoordinateX;
            _topRightCoordinateY = topRightCoordinateY;
        }

        public void MoveXForward()
        {
            if (CoordinateX < _topRightCoordinateX)
                CoordinateX++;
        }

        public void MoveXBackward()
        {
            if (CoordinateX > 0)
                CoordinateX--;
        }

        public void MoveYForward()
        {
            if (CoordinateY < _topRightCoordinateY)
                CoordinateY++;
        }

        public void MoveYBackward()
        {
            if (CoordinateY > 0)
                CoordinateY--;
        }
    }
}