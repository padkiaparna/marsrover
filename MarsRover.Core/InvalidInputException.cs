﻿using System;

namespace MarsRover.Core
{
    public class InvalidInputException : Exception
    {
        public InvalidInputException(string message): base(message)
        {

        }
    }
}
