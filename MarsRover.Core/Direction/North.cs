﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover.Core.Interfaces;

namespace MarsRover.Core.Direction
{
    public class North : IDirection
    {
        private readonly IPlateau _plateau;

        public North(IPlateau plateau)
        {
            _plateau = plateau;
        }

        public IDirection TurnLeft()
        {
            return new West(_plateau);
        }

        public IDirection TurnRight()
        {
            return new East(_plateau);
        }

        public void Move()
        {
            _plateau.MoveYForward();
        }

        public override string ToString()
        {
            return "N";
        }
    }
}
