﻿using MarsRover.Core.Direction;
using MarsRover.Core.Interfaces;
using System;

namespace MarsRover.Core.Commands
{
    public class CommandBuilder
    {
        private string _input;

        public CommandBuilder(string input)
        {
            _input = input;
        }

        public void SetRover(IPlateau plateau, IRover rover)
        {
            var roverPosition = _input;
            plateau.CoordinateX = Convert.ToInt32(roverPosition.Split(' ')[0]);
            plateau.CoordinateY = Convert.ToInt32(roverPosition.Split(' ')[1]);
            var starting_direction = roverPosition.Split(' ')[2];
            IDirection direction = GetStartingDirection(starting_direction.ToUpper(),plateau);
            rover.SetPosition(plateau, direction);
        }

        private IDirection GetStartingDirection(string starting_direction, IPlateau plateau)
        {
            IDirection _direction = null;
            if (starting_direction.Equals("N"))
            {
                _direction = new North(plateau);
            }else if (starting_direction.Equals("S"))
            {
                _direction = new South(plateau);
            }else if(starting_direction.Equals("E"))
            {
                _direction = new East(plateau);
            }else if(starting_direction.Equals("W"))
            {
                _direction = new West(plateau);
            }
            else
            {
                throw new InvalidInputException("Direction: " + starting_direction + " is invalid");
            }
            return _direction;
        }

        public ICommand GetCommand()
        {
            if (_input.Equals("M"))
            {
                return new ForwardCommand();
            }else if (_input.Equals("L"))
            {
                return new TurnLeftCommand();
            }
            else if (_input.Equals("R"))
            {
                return new TurnRightCommand();
            }
            else
            {
                throw new InvalidInputException("Instruction "+ _input);
            }
        }

    }
}
