﻿namespace MarsRover.Core.Interfaces
{
    public interface IRover
    {
        void TurnLeft();
        void TurnRight();
        void Forward();
        void SetPosition(IPlateau plateau, IDirection direction);
        void ExecuteCommands(string commandString);
        IDirection Direction
        {
            get;
        }
        int CoordinateX { get; }
        int CoordinateY { get; }
    }
}