﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover.Core.Interfaces
{
    public interface IPlateau
    {
        int CoordinateX { get; set; }
        int CoordinateY { get; set; }
        void MoveXForward();
        void MoveXBackward();
        void MoveYForward();
        void MoveYBackward();
    }
}
