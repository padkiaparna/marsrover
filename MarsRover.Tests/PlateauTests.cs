﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MarsRover.Core;
using NUnit.Framework;

namespace MarsRover.Tests
{
    [TestFixture]
    public class PlateauTests
    {
        [Test]
        public void When_Create_The_Plateau_That_Rover_Position_Will_Be_0_0()
        {
            var plateau = new Plateau(5,5);
            Assert.That(plateau.CoordinateX, Is.EqualTo(0));
            Assert.That(plateau.CoordinateY, Is.EqualTo(0));
        }
    }
}
