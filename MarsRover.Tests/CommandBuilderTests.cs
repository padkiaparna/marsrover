﻿using MarsRover.Core;
using MarsRover.Core.Commands;
using MarsRover.Core.Direction;
using MarsRover.Core.Interfaces;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarsRover.Tests
{
    [TestFixture]
    public class CommandBuilderTests
    {
        [Test]
        public void SetRover_12N_SetPosition()
        {
            //arrange
            CommandBuilder commandBuilder = new CommandBuilder("1 2 N");
            IPlateau plateau = new Plateau(5, 5);
            IRover rover = new Rover(plateau);

            //act
            commandBuilder.SetRover(plateau,rover);

            //assert
            Assert.That(rover.Direction,Is.TypeOf(typeof(North)));
            Assert.AreEqual(1, rover.CoordinateX);
            Assert.AreEqual(2, rover.CoordinateY);
        }
        [Test]
        public void SetRover_33E_SetPosition()
        {
            //arrange
            CommandBuilder commandBuilder = new CommandBuilder("3 3 E");
            IPlateau plateau = new Plateau(5, 5);
            IRover rover = new Rover(plateau);

            //act
            commandBuilder.SetRover(plateau, rover);

            //assert
            Assert.That(rover.Direction, Is.TypeOf(typeof(East)));
            Assert.AreEqual(3, rover.CoordinateX);
            Assert.AreEqual(3, rover.CoordinateY);
        }

        [Test]
        public void GetCommand_M_ReturnsMoveCommand()
        {
            CommandBuilder commandBuilder = new CommandBuilder("M");

            var command = commandBuilder.GetCommand();

            Assert.That(command, Is.TypeOf(typeof(ForwardCommand)));
        }
    }
}
