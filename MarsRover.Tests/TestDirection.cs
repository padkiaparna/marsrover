﻿using MarsRover.Core;
using MarsRover.Core.Direction;
using MarsRover.Core.Interfaces;
using NUnit.Framework;

namespace MarsRover.Tests
{
    public class TestDirection
    {
        IPlateau _plateau;

        [SetUp]
        public void Setup()
        {
            _plateau = new Plateau(5,5);
        }

        [Test]
        public void When_North_And_Turn_Left_Direction_Should_Be_West()
        {
            var direction = new North(_plateau);
            var newdirection = direction.TurnLeft();
            Assert.That(newdirection, Is.TypeOf<West>());
        }

        [Test]
        public void When_North_And_Turn_Right_Direction_Should_Be_East()
        {
            var direction = new North(_plateau);
            var newdirection = direction.TurnRight();
            Assert.That(newdirection, Is.TypeOf<East>());
        }

        [Test]
        public void When_East_And_Turn_Left_Direction_Should_Be_North()
        {
            var direction = new East(_plateau);
            var newdirection = direction.TurnLeft();
            Assert.That(newdirection, Is.TypeOf<North>());
        }

        [Test]
        public void When_East_And_Turn_Right_Direction_Should_Be_South()
        {
            var direction = new East(_plateau);
            var newdirection = direction.TurnRight();
            Assert.That(newdirection, Is.TypeOf<South>());
        }

        [Test]
        public void When_South_And_Turn_Left_Direction_Should_Be_East()
        {
            var direction = new South(_plateau);
            var newdirection = direction.TurnLeft();
            Assert.That(newdirection, Is.TypeOf<East>());
        }

        [Test]
        public void When_South_And_Turn_Right_Direction_Should_Be_West()
        {
            var direction = new South(_plateau);
            var newdirection = direction.TurnRight();
            Assert.That(newdirection, Is.TypeOf<West>());
        }

        [Test]
        public void When_West_And_Turn_Left_Direction_Should_Be_South()
        {
            var direction = new West(_plateau);
            var newdirection = direction.TurnLeft();
            Assert.That(newdirection, Is.TypeOf<South>());
        }

        [Test]
        public void When_West_And_Turn_Right_Direction_Should_Be_North()
        {
            var direction = new West(_plateau);
            var newdirection = direction.TurnRight();
            Assert.That(newdirection, Is.TypeOf<North>());
        }

        [Test]
        public void When_At_Start_And_Facing_South_And_Move_Do_Not_Pass_Boundary()
        {
            var direction = new South(_plateau);
            direction.Move();
            Assert.That(_plateau.CoordinateX, Is.EqualTo(0));
            Assert.That(_plateau.CoordinateY, Is.EqualTo(0));
        }

        [Test]
        public void When_At_Start_And_Facing_West_And_Move_Do_Not_Pass_Boundary()
        {
            var direction = new West(_plateau);
            direction.Move();
            Assert.That(_plateau.CoordinateX, Is.EqualTo(0));
            Assert.That(_plateau.CoordinateY, Is.EqualTo(0));
        }
        
        [Test]
        public void When_Moving_North_At_Y_Boundary_Do_Not_Pass_Boundary()
        {
            var direction = new North(_plateau);
            for (int i = 0; i < 10; i++)
            {
                direction.Move();
            }
            Assert.That(_plateau.CoordinateX, Is.EqualTo(0));
            Assert.That(_plateau.CoordinateY, Is.EqualTo(5));
        }

        [Test]
        public void When_Moving_East_At_X_Boundary_Do_Not_Pass_Boundary()
        {
            var direction = new East(_plateau);
            for (int i = 0; i < 10; i++)
            {
                direction.Move();
            }
            Assert.That(_plateau.CoordinateX, Is.EqualTo(5));
            Assert.That(_plateau.CoordinateY, Is.EqualTo(0));
        }
    }
}
